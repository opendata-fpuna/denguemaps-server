﻿CREATE SEQUENCE public.cd_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
CREATE TABLE public.conjunto_dato
(
  fecha_alta timestamp without time zone,
  id integer NOT NULL DEFAULT nextval('cd_id_seq'::regclass),
  usuario_alta character varying,
  semana character varying,
  anio character varying,
  nombre character varying,
  CONSTRAINT cd_id PRIMARY KEY (id)
);

CREATE SEQUENCE public.prediccion_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
CREATE TABLE prediccion
(
  cantidad bigint,
  incidencia double precision,
  cantidad_anterior bigint,
  cantidad_ante_anterior bigint,
  adm1_nombre character varying,
  adm2_nombre character varying,
  adm3_nombre character varying,
  id integer NOT NULL DEFAULT nextval('prediccion_id_seq'::regclass),
  adm1_codigo character varying,
  adm2_codigo character varying,
  adm3_codigo character varying,
  brote character varying,
  brote_sgte character varying,
  id_conjunto_dato integer,
  CONSTRAINT prediccion_id PRIMARY KEY (id),
  CONSTRAINT conjunto_dato_fk FOREIGN KEY (id_conjunto_dato)
      REFERENCES conjunto_dato (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);