package com.app.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.app.utils.tables.annotation.AttributeDescriptor;

@Entity
@Table(name = "conjunto_dato")
public class ConjuntoDato implements Serializable {

	private static final long serialVersionUID = 1L;

	@AttributeDescriptor(path = "id")
	@Id
	@SequenceGenerator(name = "cd_id_seq", schema = "public", sequenceName = "cd_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "cd_id_seq")
	@Column
	private int id;

	@AttributeDescriptor(path = "fechaAlta")
	@Column(name = "fecha_alta")
	private Date fechaAlta;

	@AttributeDescriptor(path = "nombre")
	@Column(name = "nombre")
	private String nombre;

	@AttributeDescriptor(path = "semana")
	@Column(name = "semana")
	private String semana;

	@AttributeDescriptor(path = "anio")
	@Column(name = "anio")
	private String anio;

	@AttributeDescriptor(path = "usuario")
	@Column(name = "usuario_alta")
	private String usuarioAlta;

	@OneToMany(mappedBy = "conjuntoDato", cascade = CascadeType.ALL)
	private List<Prediccion> datos;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSemana() {
		return semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public List<Prediccion> getDatos() {
		return datos;
	}

	public void setDatos(List<Prediccion> datos) {
		this.datos = datos;
	}

}
