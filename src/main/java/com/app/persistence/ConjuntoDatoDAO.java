package com.app.persistence;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;

import com.app.domain.ConjuntoDato;
import com.app.domain.Prediccion;

@Stateless
public class ConjuntoDatoDAO {

	private final static Logger logger = Logger.getLogger(ConjuntoDatoDAO.class
			.getName());

	@Inject
	EntityManager em;

	public List<ConjuntoDato> getPredicciones() {

		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(ConjuntoDato.class);

		return criteria.list();

		/*
		 * String query = "select * from client"; return
		 * em.createNativeQuery(query).getResultList();
		 */
	}

	@Transactional
	public boolean insertConjunto(ConjuntoDato conjunto) {
		logger.info("++++++++++++++++++ insertConjunto");
		// agregar unique sobre todas las columnas excepto el id en la tabla
		// reporte porque no pueden existir dos filas iguales
		// si no se puede insertar que hacer?
		try {
			em.persist(conjunto);
			for (Prediccion p : conjunto.getDatos()) {
				p.setConjuntoDato(conjunto);
				em.merge(p);
			}
			logger.info("Insertando conjunto en la BD...");
			return true;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.info("Ocurrió un error al intentar insertar el conjunto.");
			return false;
		}

	}

}
