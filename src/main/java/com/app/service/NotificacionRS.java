package com.app.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.jboss.resteasy.annotations.GZIP;

import com.app.constant.Statics;
import com.app.domain.Notificacion;
import com.app.persistence.NotificacionDAO;
import com.app.tables.facade.NotificacionTableFacade;
import com.app.utils.NativeQueryFileManager;

@Named
@RequestScoped
@Path("/rest/notificacion")
@GZIP
public class NotificacionRS {

	private final static Logger logger = Logger.getLogger(NotificacionRS.class
			.getName());

	@Inject
	NotificacionDAO notificacionDao;

	@Inject
	NotificacionTableFacade facade;

	@Inject
	private NativeQueryFileManager n;

	@GET
	@Path("/lista")
	@Produces("application/json")
	public Response getNotificaciones(@Context UriInfo uriInfo) {
		MultivaluedMap<String, String> parameters = uriInfo
				.getQueryParameters();
		logger.info("+++++++++++++++++ Servicio de listado de notificaciones filtradas");
		Map<String, String[]> finalMap = new HashMap<String, String[]>();
		Set<String> keys = parameters.keySet();
		for (String key : keys) {
			List<String> list = parameters.get(key);
			String[] a = new String[] {};
			finalMap.put(key, (String[]) list.toArray(a));
		}
		return Response.status(200).entity(facade.getResult(finalMap)).build();
	}

	@GET
	@Path("/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonNotificacionesPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de notificaciones por año");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.NOTIF_POR_ANIO, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GET
	@Path("/riesgo/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonRiesgosPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de riesgos por departamento");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.RIESGOS_DEPTO, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GET
	@Path("/riesgo/distrito/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonRiesgosDistritoPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de riesgos por distrito");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.REISGOS_DITRITOS, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GET
	@Path("/riesgo/asuncion/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonRiesgosAsuncionPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de riesgos por barrios de Asunción");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.RIESGOS_ASU, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GET
	@Path("/filtros")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNotificacionesFiltradas(@QueryParam("anio") String anio,
			@QueryParam("semana") String semana,
			@QueryParam("fechaNotificacion") String fechaNotificacion,
			@QueryParam("departamento") String departamento,
			@QueryParam("distrito") String distrito,
			@QueryParam("sexo") String sexo, @QueryParam("edad") String edad,
			@QueryParam("resultado") String resultado) {
		logger.info("+++++++++++++++++ Servicio de notificaciones filtradas");
		List<Notificacion> list = notificacionDao.getNotificacionesFiltradas(
				anio, semana, fechaNotificacion, departamento, distrito, sexo,
				edad, resultado);

		return Response.ok(list).build();
	}

	@GET
	@Path("/filtrosmapa")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonNotificacionesPorFiltros(
			@QueryParam("anio") String anio, @QueryParam("f") int f,
			@QueryParam("m") int m, @QueryParam("confirmado") int confirmado,
			@QueryParam("descartado") int descartado,
			@QueryParam("sospechoso") int sospechoso) {
		String query = "";
		if (f == 1) {
			query = query + " and sexo = 'F'";
		}
		if (m == 1) {
			query = query + " and sexo = 'M'";
		}
		if (confirmado == 1 || descartado == 1 || sospechoso == 1) {
			query = query + " and (";
			if (confirmado == 1) {
				query = query + " estado_final = 'CONFIRMADO'";
			}
			if (descartado == 1) {
				if (confirmado == 1) {
					query = query + " or ";
				}
				query = query + "estado_final = 'DESCARTADO'";
			}
			if (sospechoso == 1) {
				if (descartado == 1 || confirmado == 1) {
					query = query + " or ";
				}
				query = query + "estado_final = 'SOSPECHOSO'";
			}
			query = query + " ) ";
		}
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.NOTIF_FILTRADAS_MAP,
					anio, query);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}
	
	
	@GET
	@Path("/incidencia/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonIncidenciasPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de incidencia por departamento");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.INCIDENCIA_DEPTO, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}
	
	
	@GET
	@Path("/incidencia/distrito/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonIncidenciasDistritoPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de incidencias por distrito");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.INCIDENCIA_DISTRITOS, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}
	
	@GET
	@Path("/incidencia/asuncion/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonIncidenciasAsuncionPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de incidencias por barrio de asuncion");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.INCIDENCIA_ASUNCION, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}
	
	@GET
	@Path("/poblacion/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonPoblacionPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de poblacion por departamento");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.POBLACION_DEPTO, anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}
	
	
	@GET
	@Path("/poblacion/distrito/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonPoblacionPorDistrito(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de poblacion por distrito");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Map<String, Object>> map = new HashMap<>();
		try {
			list = n.executeNativeQueryParameters(Statics.POBLACION_DISTRITO, anio);
			for (Map<String, Object> m : list) {
				String key = m.get("departamento_codigo").toString() + m.get("distrito_codigo");
				map.put(key, m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(map).build();
	}
	
	@GET
	@Path("/poblacion/asuncion/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getJsonPoblacionPorBarrioAsu() {
		logger.info("+++++++++++++++++ Servicio de poblacion por distrito");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Map<String, Object>> map = new HashMap<>();
		try {
			list = n.executeNativeQueryParameters(Statics.POBLACION_ASUNCION);
			for (Map<String, Object> m : list) {
				String key = m.get("barrio_codigo").toString();
				map.put(key, m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(map).build();
	}
}
